use Northwind

--3. select the product name and units in stock of Laughing Lumberjack Lager, Outback Lager, Ravioli Angelo
--SELECT ProductName, UnitsInStock from Products 
--WHERE ProductName in ('Laughing Lumberjack Lager', 'Outback Lager', 'Ravioli Angelo')

--2. SELECT the row of the product whose name is Queso Cabrales
--SELECT * from Products WHERE ProductName = 'Queso Cabrales'

--1. select all the fields and rows from the products table
--SELECT * from Products


--1: Select all the order information for the customerID QUEDE
--SELECT * from Orders 
--WHERE CustomerID LIKE '%QUEDE%'

--2. Select Orders whose freight in mre than $100.00
--SELECT * from Orders
--WHERE Freight > 100.00

--3. Select the orders shipping to the USA whose frieight is between $10 and $20
--SELECT * from Orders
--WHERE Freight BETWEEN 10.00 AND 20.00

--1.  Get a list of each employee and their territories
--SELECT * FROM Employees
--INNER JOIN EmployeeTerritories
--ON Employees.EmployeeID = EmployeeTerritories.EmployeeID

--2. Get the Customer Name, Order Date, and each order detail's Productname for US customers only
SELECT CompanyName, OrderDate, ProductName
From Customers
LEFT JOIN Orders
	ON Orders.CustomerID = Customers.CustomerID
LEFT JOIN [Order Details]
	ON [Order Details].OrderID = Orders.OrderID
LEFT JOIN Products
	ON Products.ProductID = [Order Details].ProductID
WHERE Country = 'USA'

--3. Get all order information where Chai was sold
SELECT * FROM Orders
LEFT JOIN [Order Details]
	ON [Order Details].OrderID = Orders.OrderID
LEFT JOIN Products
	ON Products.ProductID = [Order Details].ProductID
WHERE ProductName = 'Chai'