USE SWCCorp

DECLARE @MinimumGrants INT
SET @MinimumGrants = 1000 
DECLARE @MaximumGrant INT
SET @MaximumGrant = 1000000

SELECT * FROM [Grant] 
WHERE Amount BETWEEN @MinimumGrants AND @MaximumGrant


CREATE PROCEDURE GetProductListByCategory 
(
@CategoryName nchar(20)
) AS 

SELECT ProductName FROM CurrentProducts
WHERE Category = @CategoryName

GO

USE SWCCorp
GO
CREATE PROCEDURE GetGrantsByEmployee 
(
@LastName nchar(100)
) AS
SELECT e.EmpID, e.FirstName, e.LastName, g.GrantName, g.Amount
FROM Employee e
	JOIN [Grant] g ON e.EmpID = g.EmpID
	WHERE e.LastName = @LastName

GO


USE SWCCorp
GO

CREATE PROCEDURE AddGrant 
(
@grantID nchar(10)
) AS
INSERT INTO [Grant] (GrantID)
VALUES (@grantID)

GO

USE SWCCorp
GO

CREATE PROCEDURE UpdateGrant 
(
@grantID nchar(10)
) AS
UPDATE [Grant]
	SET EmpID = 5
WHERE GrantID = @grantID

GO
