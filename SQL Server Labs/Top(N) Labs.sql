SELECT TOP(2) HireDate
FROM Employee
Order BY HireDate

SELECT TOP(6) WITH TIES *
FROM [Grant] 
ORDER BY Amount DESC

SELECT TOP(10) ProductName
FROM CurrentProducts
WHERE Category = 'No-Stay'
ORDER BY RetailPrice DESC