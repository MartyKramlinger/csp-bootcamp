SELECT ProductName, (UnitPrice * UnitsInStock) AS ValueStock
FROM Products
ORDER BY ValueStock DESC

SELECT (LastName + FirstName) AS NameLastFirst
From Employees
ORDER BY LastName, FirstName

SELECT ProductName, (UnitPrice * UnitsInStock) AS USDollar, (UnitPrice * UnitsInStock * 1.3) AS CanDollar
		,(UnitPrice * UnitsInStock * 0.89) AS Euro, (UnitPrice * UnitsInStock * 119.92) AS Yen
FROM Products
ORDER BY USDollar DESC

--candandian dollars, yen, euros, pesos