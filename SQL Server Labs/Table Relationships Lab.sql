USE Northwind
SELECT * From Customers c 
INNER JOIN Orders o ON c.CustomerID = o.CustomerID
INNER JOIN [Order Details] d ON o.OrderID = d.OrderID
WHERE c.CustomerID = 'AROUT'

SELECT o.OrderID, o.OrderDate, d.UnitPrice AS OrderUnitPrice, p.UnitPrice AS ProductUnitPrice, d.Quantity, d.Discount, p.ProductName
FROM Orders o
	INNER JOIN [Order Details] d ON o.OrderID = d.OrderID
	INNER JOIN Products p ON d.ProductID = p.ProductID