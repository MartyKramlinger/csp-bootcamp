USE Northwind
GO
SELECT CategoryName, MAX(UnitPrice) AS MOSTEXPENSE
FROM Products p
	INNER JOIN Categories c ON c.CategoryID = p.CategoryID
GROUP BY CategoryName
ORDER BY MOSTEXPENSE 

SELECT o.CustomerID, MAX(UnitPrice * Quantity) AS MostExpensiveOrder
FROM Customers c
	INNER JOIN Orders o ON o.CustomerID = c.CustomerID
	INNER JOIN [Order Details] d ON o.OrderID = d.OrderID
GROUP BY o.CustomerID
ORDER BY MostExpensiveOrder DESC


SELECT c.CompanyName, COUNT(o.OrderID) AS CustomerOrders
	FROM Customers c 
	INNER JOIN Orders o ON c.CustomerID = o.CustomerID
	GROUP BY c.CompanyName
	HAVING COUNT(o.OrderID) > 10

SELECT e.LastName, e.FirstName, COUNT(o.OrderID) AS GoodEmployees
	FROM Employees e
	INNER JOIN Orders o ON e.EmployeeID = o.EmployeeID
	GROUP BY e.LastName, e.FirstName
	HAVING COUNT(o.OrderID) > 100
	ORDER BY GoodEmployees DESC