SELECT * FROM [Grant]
ORDER BY GrantName

SELECT * FROM Employee
ORDER BY HireDate DESC

SELECT ProductName, Category
FROM CurrentProducts
ORDER BY RetailPrice

SELECT * FROM [Grant]
ORDER BY Amount DESC, GrantName

SELECT FirstName, LastName, City
FROM Employee LEFT JOIN Location ON Employee.LocationID = Location.LocationID
ORDER BY City 