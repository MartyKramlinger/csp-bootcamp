﻿using System;
using System.Linq;

namespace LINQ
{
    class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */
        static void Main()
        {
            //PrintOutOfStock();
            //GroupEm();
            //InStockCost3();
            //WashNameOrders();
            //ProductNames();
            //ProductsIncrease25();
            ProductNamesToUpper();
            Console.ReadLine();
        }

        //private static void PrintOutOfStock()
        //{
        //    var products = DataLoader.LoadProducts();

        //    var results = products.Where(p => p.UnitsInStock == 0);

        //    foreach (var product in results)
        //    {
        //        Console.WriteLine(product.ProductName);
        //    }
        //}

        //2. print the products that are out of stock and cost more than 3.00 per unit
        //private static void InStockCost3()
        //{
        //    var products = DataLoader.LoadProducts();

        //    var results = products.Where(p => p.UnitsInStock > 0 && p.UnitPrice > 3);

        //    foreach (var product in results)
        //    {
        //        Console.WriteLine(product.ProductName);
        //    }
        //}

        //3. print the name of the customers that live in Washington State and their orders
        //private static void WashNameOrders()
        //{
        //    var customers = DataLoader.LoadCustomers();

        //    var results = customers.Where(p => p.Region == "WA");

        //    foreach (var customer in results)
        //    {
        //        Console.WriteLine(customer.CompanyName);

        //        foreach (var o in customer.Orders)
        //        {
        //            Console.WriteLine("The order ID number is {0} \nThe date of the order is {1}. \nThe total cost of the order is {2}.", o.OrderID, o.OrderDate, o.Total);
        //        }
        //    }
        //}

        //4. Create a new sequence with just the names of the products.
        //private static void ProductNames()
        //{
        //    var products = DataLoader.LoadProducts();
        //    var result = products.Where(p => p.ProductName == p.ProductName);

        //    foreach (var o in result)
        //    {
        //        Console.WriteLine(o.ProductName);
        //    }
        //}

        //5. Create a new sequence of products and unit prices where the unit prices are increased by 25%.
        //private static void ProductsIncrease25()
        //{
        //    var products = DataLoader.LoadProducts();
        //    var nameResult = products.Where(p => p.ProductName == p.ProductName);
        //    var priceResult = products.Where(u => u.UnitPrice == u.UnitPrice);
        //    foreach (var p in nameResult)
        //    {
        //        var newprice = Convert.ToInt64(p.UnitPrice);
        //        var newprice2 = newprice * 1.25;
        //        Console.WriteLine("{0, -40}     {1}", p.ProductName, newprice2);
        //    }
        //    Console.ReadLine();
        //}

        //6. Create a new sequence of just product names in all upper case.
        private static void ProductNamesToUpper()
        {
            var products = DataLoader.LoadProducts();
            var result = products.Where(p => p.ProductName == p.ProductName);
            foreach (var r in result)
            {
                Console.WriteLine(r.ProductName.ToUpper());
            }
        }

        // 24. Group customer orders by year, then by month.
        //private static void GroupEm()
        //{
        //    var customers = DataLoader.LoadCustomers();
        //    var result = from customer in customers
        //                 from order in customer .Orders
        //                 group order by order.OrderDate.Year into yr
        //                 select new
        //                 {
        //                     Year = yr.Key,
        //                     MonthGroups = from yrs in yr
        //                                   group yrs by yrs.OrderDate.Month into mnths
        //                                   select new { Month = mnths.Key, Orders = mnths}
        //                 };

        //    var a = "a";

        //}
    }
}
