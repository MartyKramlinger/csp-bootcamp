﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MNSportsDB
{
    class MNSportsRepository : IRepository
    {
        public Champ GetChampById(int id)
        {
            using (MinneotaSportsTeamsEntities ctx = new MinneotaSportsTeamsEntities())
            {
                var champs = ctx.Champs;
                return champs.FirstOrDefault(x => x.TeamID == id);
            }
        }

        public Team GetTeamById(int? id)
        {
            using (MinneotaSportsTeamsEntities ctx = new MinneotaSportsTeamsEntities())
            {
                var teams = ctx.Teams;
                return teams.FirstOrDefault(x => x.TeamID == id);
            }
        }
        public static Team GetTeamByName(string team)
        {
            using(MinneotaSportsTeamsEntities ctx = new MinneotaSportsTeamsEntities())
            {
                var teams = ctx.Teams;
                return teams.FirstOrDefault(x => x.Teamname == team);
            }
        }
        public Venue GetVenuebyId(int? id)
        {
            using (MinneotaSportsTeamsEntities ctx = new MinneotaSportsTeamsEntities())
            {
                Venue venue = new Venue();
                var venues = ctx.Venues;
                return venues.FirstOrDefault(x => x.VenueID == id);
            }
        }

        public Year GetYearById(int? id)
        {
            using (MinneotaSportsTeamsEntities ctx = new MinneotaSportsTeamsEntities())
            {
                Year year = new Year();
                var years = ctx.Years;
                return years.FirstOrDefault(x => x.YearsID == id);
            }
        }

        public static void AddTeam(Team team)
        {
            using(MinneotaSportsTeamsEntities ctx = new MinneotaSportsTeamsEntities())
            {
                ctx.Teams.Add(team);
            }
        }

        public static void AddChamp(Champ champ)
        {
            using(MinneotaSportsTeamsEntities ctx = new MinneotaSportsTeamsEntities())
            {
                ctx.Champs.Add(champ);
            }
        }

        public static void AddVenue(Venue venue)
        {
            using(MinneotaSportsTeamsEntities ctx = new MinneotaSportsTeamsEntities())
            {
                ctx.Venues.Add(venue);
            }
        }

        public static void DeleteTeam(Team team)
        {
            using (MinneotaSportsTeamsEntities ctx = new MinneotaSportsTeamsEntities())
            {
                ctx.Teams.Remove(team);
            }
        }

        public static void DeleteChamp(Champ champ)
        {
            using (MinneotaSportsTeamsEntities ctx = new MinneotaSportsTeamsEntities())
            {
                ctx.Champs.Remove(champ);
            }
        }
        public static void DeleteVenue(Venue venue)
        {
            using (MinneotaSportsTeamsEntities ctx = new MinneotaSportsTeamsEntities())
            {
                ctx.Venues.Remove(venue);
            }
        }

        public static void EditTeam(Team team)
        {
            DeleteTeam(team);
            AddTeam(team);
        }

        public static void EditChamp(Champ champ)
        {
            DeleteChamp(champ);
            AddChamp(champ);
        }

        public static void EditVenue(Venue venue)
        {
            DeleteVenue(venue);
            AddVenue(venue);
        }
    }
}
