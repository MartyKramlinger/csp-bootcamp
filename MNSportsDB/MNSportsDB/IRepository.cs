﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MNSportsDB
{
    public interface IRepository
    {
        Team GetTeamById(int id);
        Champ GetChampById(int id);
        Venue GetVenuebyId(int id);
        Year GetYearById(int id);
    }
}
