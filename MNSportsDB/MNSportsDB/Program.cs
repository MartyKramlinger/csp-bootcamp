﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MNSportsDB
{
    class Program
    {
        static Team displayedTeam;
        static void Main(string[] args)
        {
        }

        public static void DisplayInfo()
        {
            MNSportsRepository repo = new MNSportsRepository();
            bool teamIsNull = false;
            do
            {
                Console.WriteLine("\tPlease enter the name of the team whose \n\tinformation you would like to display.");
                string team = Console.ReadLine();
                Team displayedTeam = MNSportsRepository.GetTeamByName(team);
                if (displayedTeam == null)
                {
                    teamIsNull = true;
                }

            } while (teamIsNull == false);
            
            Console.WriteLine("Name: " + displayedTeam.Teamname);
            Console.WriteLine("Sport: " + displayedTeam.Sport);
            Console.WriteLine("Owner: " + displayedTeam.Owner);
            Console.WriteLine("Mascot: " + displayedTeam.Mascot);
            Console.WriteLine("Coach: " + displayedTeam.Coach);
            Console.WriteLine("Still active?" + displayedTeam.Active);
            Console.WriteLine("Average salarary: " + displayedTeam.AverageSalary);
            Console.WriteLine("Number of total indidivdual convictions: " + displayedTeam.Convictions);
            Venue venue = repo.GetVenuebyId(displayedTeam.VenueID);
            if (venue == null)
            {
                Console.WriteLine("We couldn't find their venue, sorry!");
            }
            else
            {
                Console.WriteLine($"Venue: {venue.Building} in {venue.City}.");
            }
            
            Year year = repo.GetYearById(displayedTeam.YearsID);
            if (year == null)
            {
                Console.WriteLine("We couldn't find info on their active years, sorry!");
            }
            else
            {
                Console.WriteLine($"Year founded: {year.YearFounded}. \nYear disbanded: {year.YearDisbanded}");
            }
        }

        public static void AddTeam()
        {
            Team team = new Team();
            Console.WriteLine("What is the name of the team you would like to add?");
            team.Teamname = Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"What type of sport do the {team.Teamname} play?");
            team.Sport = Console.ReadLine();
            Console.ReadLine();
            Console.WriteLine($"Who is the {team.Teamname}'s coach?");
            team.Coach = Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"Who is the {team.Teamname}'s main owner?");
            team.Owner = Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"Who is the {team.Teamname}'s mascot?");
            team.Mascot = Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"How many convictions does the team have");

        }
    }
}
