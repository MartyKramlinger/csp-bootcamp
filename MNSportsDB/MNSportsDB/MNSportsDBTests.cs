﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace MNSportsDB
{
    class MNSportsDBTests
    {
        [TestCase ()]
        private static void GetTeamByNameTest(string name)
        {
            MNSportsRepository repo = new MNSportsRepository();
            Team actual = MNSportsRepository.GetTeamByName(name);

            Assert.IsNotNullOrEmpty(actual.Owner);
        }
    }
}
