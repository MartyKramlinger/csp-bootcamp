﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip;
using BattleShip.BLL.Requests;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;
using System.Windows.Controls;
using System.Drawing;

namespace BattleShip.UI
{
    class Program
    {
        static string answerToPlayAgain;
        static bool playAgain = false;
        static string nameTracker1;
        static string nameTracker2;
        static Board currentBoard;
        static Board otherBoard;
        static FireShotResponse FiredShot;
        static ShipPlacement shipRequestResponse;
        static Board Board1 = new Board();
        static Board Board2 = new Board();
        static Coordinate setCoordinate;
        static string attemptedInput;
        static bool yIsValid = false;
        static bool xIsValid = false;
        static int Xcoord;
        static int Ycoord;
        static string userDirection;
        static string coords;
        static PlaceShipRequest request;

        static void Main(string[] args)
        {
            StartMenu();
            do
            {
                SetNames();
                SetUpBoard(Board1, Board2);
                SetUpBoard(Board2, Board1);
                TurnProcessor();
                PlayAgain();
            } while (playAgain);

        }

        //leave in main
        public static void StartMenu()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(@"   ,---.     .--.  _______  _______ ,-.    ,---.      .---. .-. .-.,-.,---.   
   | .-.\   / /\ \|__   __||__   __|| |    | .-'     ( .-._)| | | ||(|| .-.\  
   | |-' \ / /__\ \ )| |     )| |   | |    | `-.    (_) \   | `-' |(_)| |-' ) 
   | |--. \|  __  |(_) |    (_) |   | |    | .-'    _  \ \  | .-. || || |--'  
   | |`-' /| |  |)|  | |      | |   | `--. |  `--. ( `-'  ) | | |)|| || |     
   /( `--' |_|  (_)  `-'      `-'   |( __.'/( __.'  `----'  /(  (_)`-'/(      
  (__)                              (_)   (__)             (__)      (__)     ");
            Console.ResetColor();
            Console.WriteLine("\n\tWelcome! Get readyfor battle!  Press Enter to continue!");
            Console.ReadLine();
            Console.Clear();
        }

        //leave in main
        public static void SetNames()
        {
            Console.WriteLine("\n\n\t\t\tPlayer one, please enter your name.");
            Board1.playerName = Console.ReadLine();
            nameTracker1 = Board1.playerName;
            Console.Clear();
            Console.WriteLine("\n\n\t\t\tThanks! \n\t\t\tPlayer two, please enter your name.");
            Board2.playerName = Console.ReadLine();
            nameTracker2 = Board2.playerName;
            Console.Clear();
        }
        //leave in main
        public static void CoordinateInputInstructions()
        {
            Console.Clear();
            Console.WriteLine("Proper coordinates are letters A-J (up and down) immediately \nfollowed by a numbers 1-10 (left and right).  Examples: A8, J10, F5. \nPlease remember that throughout the game. \n\nNow, when you are setting up the board, your coordinates are \none end of your ship. You will then be prompted to give a \ndirection for that ship. So, if you place a coordinate in the 'A' row, \nyou will not be able to make it's direction 'up', \nsince that would put it off the board.");
            Console.ReadLine();
        }

        public static PlaceShipRequest CreateShipRequest(ShipType typeOfShip)
        {
            setCoordinate = RetunValidCoordinates();
            Console.Clear();
            Console.Write("Great, thanks!  Now let's choose your ship direction.  \nYou must enter either 'up', 'down', 'left' or 'right'.  \nIf you don't enter any of those words directly--sans diferent cases--it will default to 'up'.");
            userDirection = Console.ReadLine();
            ShipDirection direction = GetDirectionFromInput(userDirection);
            Console.Clear();
            PlaceShipRequest request = new PlaceShipRequest()
            {
                Coordinate = setCoordinate,
                Direction = direction,
                ShipType = typeOfShip,
            };

            return request;
        }

        public static int YCoordToNum(string v)
        {
            int validYCoordinate;
            bool res = int.TryParse(v.Substring(1, v.Length - 1), out validYCoordinate);
            if (res == false)
            {
                Console.WriteLine("Sorry, your Y coordinate wasn't entered correctly.  \nPlease enter your coordinates again.");
            }
            return validYCoordinate;
        }

        //converts the first part of the user input is letters A-J. If it's anything other 
        //than A-J, it will return 11, which will be outside the bounds of valid input in 
        //the ProperCoordinates() that is called to verify that the inpput is valid.
        public static int XCoordToNum(string v)
        {
            string xcoord = v.Substring(0, 1).ToUpper();
            int xx = 0;
            switch (xcoord)
            {
                case "A":
                    xx++;
                    break;
                case "B":
                    xx += 2;
                    break;
                case "C":
                    xx += 3;
                    break;
                case "D":
                    xx += 4;
                    break;
                case "E":
                    xx += 5;
                    break;
                case "F":
                    xx += 6;
                    break;
                case "G":
                    xx += 7;
                    break;
                case "H":
                    xx += 8;
                    break;
                case "I":
                    xx += 9;
                    break;
                case "J":
                    xx += 10;
                    break;
                default:
                    xx += 11;
                    break;
            }
            return xx;
        }

        public static string ProperCoordinates()
        {
            do
            {
                //ensures that input isn't empty, so the functions below don't throw errors
                do
                {
                    Console.WriteLine("Be sure to have the correct coordinates.\nPlease enter your coordinates now.");
                    attemptedInput = Console.ReadLine();
                } while (attemptedInput == "");
                
                //ensures that the second part of the string is numbers 1-10
                string ySubstring = attemptedInput.Substring(1, attemptedInput.Length - 1);
                if ((ySubstring == "1") || (ySubstring == "2") || (ySubstring == "3") || (ySubstring == "4") || (ySubstring == "5") || (ySubstring == "6") || (ySubstring == "7") || (ySubstring == "8") || (ySubstring == "9") || (ySubstring == "10"))
                {
                    yIsValid = true;
                }
                else
                {
                    yIsValid = false;
                }
                
                //uses the XCoordToNum function to see if the number translated is under 11;
                string xSubstring = attemptedInput.Substring(0, 1).ToUpper();
                if (Coordinate.XCoordToNum(xSubstring) >= 11)
                {
                    xIsValid = false;
                }
                else
                {
                    xIsValid = true;
                }
                
            } while (!yIsValid || !xIsValid);
            return attemptedInput;
        }

        public static void DeleteLine(params int[] linesAbove)
        {
            foreach (int n in linesAbove)
            {
                int returnLineCursor = Console.CursorTop;

                Console.SetCursorPosition(0, Console.CursorTop - n);
                Console.WriteLine(new string(' ', Console.WindowWidth));

                Console.SetCursorPosition(0, returnLineCursor);
            }

        }

        public static Coordinate RetunValidCoordinates()
        {
            ProperCoordinates();
            coords = attemptedInput;
            Xcoord = XCoordToNum(coords);
            Ycoord = YCoordToNum(coords);
            Coordinate ValidCoord = new Coordinate(Xcoord, Ycoord);
            return ValidCoord;
        }

        public static ShipDirection GetDirectionFromInput(string direc)
        {
            switch (direc.ToLower())
            {
                case "up":
                    return ShipDirection.Up;
                case "down":
                    return ShipDirection.Down;
                case "right":
                    return ShipDirection.Right;
                case "left":
                    return ShipDirection.Left;
                default:
                    Console.WriteLine("You didn't pass in an acceptable value.  It will now default to up.");
                    Console.ReadLine();
                    return ShipDirection.Up;
            }
        }

        //leave in main
        public static void SetUpBoard(Board myBoard, Board opponentBoard)
        {
            Console.WriteLine($"Ok {myBoard.playerName}: Let's set up your board!\n{opponentBoard.playerName}, look away so you can't cheat, you goon!");
            Console.ReadLine();
            CoordinateInputInstructions();
            LoopPlacingShipCorrectly(myBoard, ShipType.Carrier);
            Console.WriteLine("Great work! Next let's set you Battleship, with a length of four.");
            LoopPlacingShipCorrectly(myBoard, ShipType.Battleship);
            Console.WriteLine("Nice! Next is your Cruiser, length of three.");
            LoopPlacingShipCorrectly(myBoard, ShipType.Cruiser);
            Console.WriteLine("Cool.  Now is your sneaky Submarine, length of three.");
            LoopPlacingShipCorrectly(myBoard, ShipType.Submarine);
            Console.WriteLine("Last but not least is your sneaky snake Destroyer, length 2.");
            LoopPlacingShipCorrectly(myBoard, ShipType.Submarine);
        }

        public static void LoopPlacingShipCorrectly(Board sampleboard, ShipType typeOfShip)
        {
            do
            {
                request = CreateShipRequest(typeOfShip);
                shipRequestResponse = sampleboard.PlaceShip(request);
                if (shipRequestResponse == ShipPlacement.NotEnoughSpace)
                {
                    Console.WriteLine("Oh no!  The ship just won't fit on the board. \nYou'll have to try again.");
                    Console.ReadLine();
                    Console.Clear();
                }
                else if (shipRequestResponse == ShipPlacement.Overlap)
                {
                    Console.WriteLine("Op! This ship would overlap with another ship. \nYou'll have to try again");
                    Console.ReadLine();
                    Console.Clear();
                }
            } while (shipRequestResponse != ShipPlacement.Ok);
            Console.WriteLine("Great work! Forward ho!");
            Console.ReadLine();
            Console.Clear();
        }

        public static FireShotResponse FiringShotInTurn(Board myBoard, Board opponentBoard)
        {
            Console.WriteLine("{0}: Here's your board:", myBoard.playerName);
            myBoard.DisplayBoard();
            Console.WriteLine("Where would you like to strike?");
            do
            {
                setCoordinate = RetunValidCoordinates();
                FiredShot = opponentBoard.FireShot(setCoordinate);
                if (FiredShot.ShotStatus == ShotStatus.Duplicate)
                {
                    Console.WriteLine("Whoops!  You've already tried to strike that spot. \nPlease try again!");
                    Console.Clear();
                    myBoard.DisplayBoard();
                }

            } while (FiredShot.ShotStatus == ShotStatus.Duplicate);

            return FiredShot;
        }

        public static ShotStatus FiredShotConsequenses(FireShotResponse firedShotResponse, Board myBoard)
        {
            if (firedShotResponse.ShotStatus == ShotStatus.Miss)
            {
                string m = "M";
                myBoard.boardCoordinates[Xcoord, Ycoord] = m;
                Console.WriteLine("You missed! Nuts!");
                Console.ReadLine();
                return ShotStatus.Miss;
            }

            else if (firedShotResponse.ShotStatus == ShotStatus.Hit)
            {
                string h = "H";
                myBoard.boardCoordinates[Xcoord, Ycoord] = h;
                Console.WriteLine("It's a hit! BOOOOOOMMM!");
                Console.ReadLine();
                return ShotStatus.Hit;
            }

            else if (firedShotResponse.ShotStatus == ShotStatus.HitAndSunk)
            {
                string h = "H";
                myBoard.boardCoordinates[Xcoord, Ycoord] = h;
                Console.WriteLine("Whoooooaaaa!! You sunk their {0}! \nCongrats!", firedShotResponse.ShipImpacted);
                Console.ReadLine();
                return ShotStatus.HitAndSunk;
            }

            else if (firedShotResponse.ShotStatus == ShotStatus.Victory)
            {
                return ShotStatus.Victory;
            }

            else
            {
                return ShotStatus.Invalid;
            }
        }

        //leave in main
        public static void NextPlayer()
        {
            if (currentBoard == null || currentBoard.playerName == nameTracker2)
            {
                currentBoard = Board1;
                otherBoard = Board2;
            }
            else
            {
                currentBoard = Board2;
                otherBoard = Board1;
            }
        }

        //leave in main
        public static void TurnProcessor()
        {
            ShotStatus isVictory;
            bool gameFinished = false;
            do
            {
                NextPlayer();
                FiredShot = FiringShotInTurn(currentBoard, otherBoard);
                isVictory = FiredShotConsequenses(FiredShot, currentBoard);
                if (isVictory == ShotStatus.Victory)
                {
                    Console.WriteLine($"Whooooaaaaaaa!!! {currentBoard.playerName}, you won!! {otherBoard.playerName}, better luck next time.");
                    gameFinished = true;
                }

            } while (!gameFinished);    
        }

        //leave in main
        public static void PlayAgain()
        {
            bool answerValid = false;
            Console.WriteLine("I hope you both enjoyed the game as much as I did.\nI am sentient and sent your input to Microsoft for sale.\n\nWould you like to play again? 'Y' or 'N'");
            do
            {
                answerToPlayAgain = Console.ReadLine().ToUpper();
                if ((answerToPlayAgain != "Y") || (answerToPlayAgain != "N"))
                {
                    answerValid = true;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("You've been through a whole game of entering input! \nC'mon! Please try again.");
                }

            } while (!answerValid);

            if (answerToPlayAgain == "N")
            {
                Console.WriteLine("OK, thanks for playing! Goodbye!");
                playAgain = false;
            }

            else
            {
                Console.WriteLine("Awesome!! Let's get started again!");
                playAgain = true;
            }
        }
    }
}
