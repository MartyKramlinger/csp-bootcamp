﻿using System;
using BattleShip;

namespace BattleShip.BLL.Requests
{
    public class Coordinate
    {
        private int _YCoordinate;
        private int _XCoordinate;
        public int XCoordinate {
            get
            {
                return _XCoordinate;
            }
            set
            {
                if (value > 10 || value < 1)
                {
                    _XCoordinate = 1;
                    //or you could throw an error
                    throw new ArgumentException();
                }
                else
                {
                    _XCoordinate = value;
                }
            }
        }
        public int YCoordinate {
            get
            {
                return _YCoordinate;
            }
            set
            {
                if (value > 10 || value < 1)
                {
                    _YCoordinate = 1;
                }
                else
                {
                    _YCoordinate = value;
                }
            }
        }
        //these _XCoordinate and _Ycoordinate equal their opposite
        //values only as written. this is because I wrote the methods wrong (switched),
        //but the quick and fine switch was simply to reverse these values.
        public Coordinate(int xCoordinate, int yCoordinate) //"B10"
        {
            //XCoordinate = _XCoordinate;
            //YCoordinate = _YCoordinate;
            _XCoordinate = xCoordinate;
            _YCoordinate = yCoordinate;
        }

        public override bool Equals(object obj)
        {
            Coordinate otherCoordinate = obj as Coordinate;

            if (otherCoordinate == null)
                return false;

            return otherCoordinate.XCoordinate == this.XCoordinate &&
                   otherCoordinate.YCoordinate == this.YCoordinate;
        }
		
		public override int GetHashCode() 
        { 
            string uniqueHash = this.XCoordinate.ToString() + this.YCoordinate.ToString() + "00"; 
            return (Convert.ToInt32(uniqueHash)); 
        }
         
        public static int XCoordToNum(string userInput)
        {
            string xcoord = userInput.Substring(0, 1);
            int xx = 0;
            switch (xcoord)
            {
                case "A":
                    xx++;
                    break;
                case "B":
                    xx += 2;
                    break;
                case "C":
                    xx += 3;
                    break;
                case "D":
                    xx += 4;
                    break;
                case "E":
                    xx += 5;
                    break;
                case "F":
                    xx += 6;
                    break;
                case "G":
                    xx += 7;
                    break;
                case "H":
                    xx += 8;
                    break;
                case "I":
                    xx += 9;
                    break;
                case "J":
                    xx += 10;
                    break;
                default:
                    xx += 11;
                    break;
            }
            return xx;
        }

        public static int ParsedYCoord(string userInput)
        {
            int yy = int.Parse(userInput.Substring(1, userInput.Length - 1));
            return yy;
        }
    }
}
