﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintWednesdays
{
    class Program
    {
        private static int NumWeeks;
        static void Main(string[] args)
        {
            Console.WriteLine("How many upcoming Wednesdays' dates would you like to print?");
            PrintWednesdays(GoToWednesday(), ParseWeeks());
            Console.ReadLine();
        }
        
        public static void DateToday()
        {
            Console.WriteLine(DateTime.Today.DayOfWeek);
        }
        
        public static DateTime GoToWednesday()
        {
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
                return DateTime.Today.AddDays(0);
            else if (DateTime.Today.DayOfWeek == DayOfWeek.Tuesday)
                return DateTime.Today.AddDays(1);
            else if (DateTime.Today.DayOfWeek == DayOfWeek.Monday)
                return DateTime.Today.AddDays(2);
            else if (DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                return DateTime.Today.AddDays(3);
            else if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday)
                return DateTime.Today.AddDays(-3);
            else if (DateTime.Today.DayOfWeek == DayOfWeek.Friday)
                return DateTime.Today.AddDays(-2);
            else
                return DateTime.Today.AddDays(-1);

        }

        public static void PrintWednesdays(DateTime DateTime, int NumWeeks)
        {
            Console.WriteLine("The upcoming Wednesday's date is {0}", DateTime.AddDays(7).ToString("MMM d yyyy"));
            for (int i = 1; i < NumWeeks; i++)
            {
                int futureWeds = i * 7;
                Console.WriteLine("The date of the following Wednesday is {0}", DateTime.AddDays(futureWeds + 7).ToString("MMM d yyyy"));
            }
        }
        
        public static int ParseWeeks()
        {
            string input = Console.ReadLine();
            return NumWeeks = int.Parse(input);
        }
    }
}
