﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountingRolls
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberCounter();
        }

        public static void NumberCounter()
        {
            int one = 0;
            int two = 0;
            int three = 0;
            int four = 0;
            int five = 0;
            int six = 0;
            Random rnd = new Random();
            for (int i = 0; i < 100; i++)
            {
                int radom = rnd.Next(1, 7);
                if (radom == 1)
                    one++;
                else if (radom == 2)
                    two++;
                else if (radom == 3)
                    three++;
                else if (radom == 4)
                    four++;
                else if (radom == 5)
                    five++;
                else
                    six++;
            }
            Console.WriteLine("Out of 100 rolls:");
            Console.WriteLine("One was rolled {0} times.", one);
            Console.WriteLine("Two was rolled {0} times.", two);
            Console.WriteLine("Three was rolled {0} times.", three);
            Console.WriteLine("Four was rolled {0} times.", four);
            Console.WriteLine("Five was rolled {0} times.", five);
            Console.WriteLine("Six was rolled {0} times.", six);
            Console.ReadLine();   
        }

    }
}
