﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Reverse_String
{
    [TestFixture]
    class ReverseStringTest
    {
      [TestCase("Hello", "olleH")]
      [TestCase("yeppers", "sreppey")]
      [TestCase("Hello Alec", "celA olleH")]
      public void ReverseArrayTest(string str, string expected)
      {
          ReverseArrayClass obj = new ReverseArrayClass();

          string actual = obj.ReverseArray(str);

          Assert.AreEqual(expected, actual);   
      }
    }
}
