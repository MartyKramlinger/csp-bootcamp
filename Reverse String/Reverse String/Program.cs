﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reverse_String
{
    class Program
    {
        string neword;
        string word;
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter any text. I will return it to you in reverse order.");
            string word = Console.ReadLine();
            Console.WriteLine(ReverseArray(word));
            Console.ReadLine();
        }
        public static string ReverseArray(string str)
        {
            if (str == null)
                return "String was null";
            else
            {
                char[] arr = str.ToCharArray();
                Array.Reverse(arr);
                return new string(arr);
            }   
            
        }
    }
}
