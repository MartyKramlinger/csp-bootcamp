﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reverse_String
{
    class ReverseArrayClass
    {
        //I ran into difficulties testing the code as is it written in the main with the 
        //static keyword in front of the method.  I tried to troubleshoot but the easier 
        //thing to do was to just make it again in this class, and test this function
        //even though the function called in the main's definition is simply in the program class.
        public string ReverseArray(string str)
        {
            if (str == null)
                return "String was null";
            else
            {
                char[] arr = str.ToCharArray();
                Array.Reverse(arr);
                return new string(arr);
            }

        }
    }
}
