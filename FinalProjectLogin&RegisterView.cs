@model SWC_LMS.Models.LMSLoginViewModel

@{
    Layout = "~/Views/Shared/_LoginLayout.cshtml";
    ViewBag.Title = "Login - EBONI VERSION";
}

<div style="text-align:center">
    <h4>Software Craftsmanship Guild - Learning Management System</h4>
    </div>

    <div style="margin-top:30px"></div>

    <div class"container-fluid">
        <div class="row row-centered">

            <!-- ExistingUsersLogin-->

            @using (Html.BeginForm("Login", "Home", FormMethod.Post, new { @class = "form-horizontal", role = "form" }))
            {
                <div class="col-md-6 col-md-offset-2">

                    <div class="col-md-6">
                        <fieldset class="scheduler-border">
                            <h5>Existing Users Log In Here</h5>
                            <!-- Email--->
                            <div class="control-group">
                                @Html.LabelFor(x => x.Email, "Email", new { @class = "control-label", @for = "email" })
                                <div class="controls">
                                    @Html.TextBoxFor(x => x.Email, new { id = "email", type = "text", placeholder = "", @class = "input-md", required = "" })
                                </div>
                            </div>

                            <!-- Password-->
                            <div class="control-group">
                                @Html.LabelFor(x => x.Password, "Password", new { @class = "control-label", @for = "password" })
                                <div class="controls">
                                    @Html.TextBoxFor(x => x.Password, new { id = "password", type = "text", placeholder = "", @class = "input-md", required = "" })
                                </div>
                            </div>

                            <!-- Button -->
                            <div style="margin-top:15px"></div>
                            <div class="controls">
                                <div class="control-group" style="text-align:center">
                                    <button id="dbtn" name="dbtn" class="btn btn-primary">Login</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            }

            <!-- NewUserRegistration-->


            @using (Html.BeginForm("Login", "Home", FormMethod.Post, new { @class = "form-horizontal", role = "form" }))
            {
                <div class="col-md-6 col-md-offset-2">

                    <div class="col-md-6">
                        <fieldset class="scheduler-border">
                            <h5>New User? Register Now!</h5>
                            <!-- First Name--->
                            <div class="control-group">
                                @Html.LabelFor(x => x.FirstName, "First Name", new { @class = "control-label", @for = "fName" })
                                <div class="controls">
                                    @Html.TextBoxFor(x => x.FirstName, new { id = "fName", type = "text", placeholder = "", @class = "input-md", required = "" })
                                </div>
                            </div>

                            <!-- Last Name-->
                            <div class="control-group">
                                @Html.LabelFor(x => x.LastName, "Last Name", new { @class = "control-label", @for = "lName" })
                                <div class="controls">
                                    @Html.TextBoxFor(x => x.LastName, new { id = "lName", type = "text", placeholder = "", @class = "input-md", required = "" })
                                </div>
                            </div>

                            <!-- Email-->
                            <div class="control-group">
                                @Html.LabelFor(x => x.Email, "Email", new { @class = "control-label", @for = "newEmail" })
                                <div class="controls">
                                    @Html.TextBoxFor(x => x.Email, new { id = "newEmail", type = "text", placeholder = "", @class = "input-md", required = "" })
                                </div>
                            </div>

                            <!-- Password-->
                            <div class="control-group">
                                @Html.LabelFor(x => x.Password, "Password", new { @class = "control-label", @for = "newPass" })
                                <div class="controls">
                                    @Html.TextBoxFor(x => x.Password, new { id = "newPass", type = "text", placeholder = "", @class = "input-md", required = "" })
                                </div>
                            </div>

                            <!-- Confirm Password-->
                            <div class="control-group">
                                @Html.LabelFor(x => x.ConfirmPassword, "Confirm Password", new { @class = "control-label", @for = "chkPass" })
                                <div class="controls">
                                    @Html.TextBoxFor(x => x.ConfirmPassword, new { id = "chkPass", type = "text", placeholder = "", @class = "input-md", required = "" })
                                </div>
                            </div>

                            <!-- Suggested Role-->
                            <div class="control-group">
                                @Html.LabelFor(x => x.SuggestedRole, "I am a...", new { @class = "control-label", @for = "sugRole" })
                                <div class="controls archivebox-form">
                                    @Html.DropDownListFor(x => x.SuggestedRole, new List<SelectListItem>{
                                                    new SelectListItem { Text = "Teacher", Value = "Teacher" },
                                                    new SelectListItem { Text = "Parent", Value = "Parent" },
                                                    new SelectListItem { Text = "Student", Value = "Student" }},
                                                    "Choose one",
                                                    new { id = "sugRole", name = "sugRole", @class = "input-md", required = "" })
                                </div>
                            </div>

                        <!-- Grade Level -->
                        <div class="control-group">
                            @Html.LabelFor(x => x.GradeLevelName, "Grade Level", new { @class = "control-label", @for = "gradeLevel" })
                            <div class="controls archivebox-form">
                                @Html.DropDownListFor(x => x.GradeLevelName, new List<SelectListItem>{
                                     new SelectListItem { Text = "K", Value = "K" },
                                     new SelectListItem { Text = "1st Grade", Value = "1st Grade" },
                                     new SelectListItem { Text = "2nd Grade", Value = "2nd Grade" },
                                     new SelectListItem { Text = "3rd Grade", Value = "3rd Grade" },
                                     new SelectListItem { Text = "4th Grade", Value = "4th Grade" },
                                     new SelectListItem { Text = "5th Grade", Value = "5th Grade" },
                                     new SelectListItem { Text = "6th Grade", Value = "6th Grade" },
                                     new SelectListItem { Text = "7th Grade", Value = "7th Grade" },
                                     new SelectListItem { Text = "8th Grade", Value = "8th Grade" },
                                     new SelectListItem { Text = "Freshman", Value = "Freshman" },
                                     new SelectListItem { Text = "Sophmore", Value = "Sophmore" },
                                     new SelectListItem { Text = "Junior", Value = "Junior" },
                                     new SelectListItem { Text = "Senior", Value = "Senior" }},
                                     "N/A",
                                     new { id = "gradeLevel", name = "gradeLevel", @class = "input-md", required = "" })
                            </div>
                            </div>

                            <!-- Button -->
                            <div style="margin-top:25px"></div>
                            <div class="controls">
                                <div class="control-group" style="text-align:center">
                                    <button id="dbtn" name="dbtn" class="btn btn-primary">Register</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            }
        </div>
    </div>