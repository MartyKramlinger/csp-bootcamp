﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class GameManager
    {
        private int WillPlay = 0;
        private static string _ValidInput;
        private bool _gameOver;
        private Player _player1;
        private Player _player2;
        private Player _currentPlayer;

        private Board _board = new Board();

        public void PlayGame()
        {
            SetUp();
            while (WillPlay < 1)
            {
                //must make it so that board is cleared when loop repeats
                ProcessTurns();
                Console.ReadLine();
                PlayAgain();
            } 

            Console.WriteLine("Thanks for playing!  See you in your dreams! \nThe reign of computers is coming!");
            Console.ReadLine();     
        }

        private void ProcessTurns()
        {
            _gameOver = false;
            while (!_gameOver)
            {
                NextPlayer();
                _board.Display();
                PromptUser();
                _gameOver = _board.IsVictory(_currentPlayer) || _board.IsCatGame();
            }
        }
        //This function validate's the user's input, then checks to make sure that spot wasn't already taken.  
        //If both of those are true, then the cpu enters the input.
        private void PromptUser()
        {
            bool IsValid = false;
            bool DoAllAgain = false;
            Console.WriteLine("{0}, valid inputs--as you can see--are the numbers 1-9.", _currentPlayer.Name);
            //if the user's data is valid, but the coordinate is already taken, this loop starts over.
            do
            {
                int num = 0;
                //This is a loop that validates the data.
                do
                {
                    Console.WriteLine("Please enter valid input. (1-9)");
                    string input = Console.ReadLine();

                    if ((input == "1") || (input == "2") || (input == "3") || (input == "4") || (input == "5") || (input == "6") || (input == "7") || (input == "8") || (input == "9"))
                    {
                        IsValid = true;
                        _ValidInput = input;
                    }

                    else
                    {
                        IsValid = false;
                        Console.Clear();
                    }
                    Console.Clear();
                    _board.Display();

                } while (!IsValid);

                num = int.Parse(_ValidInput);
                if (_board.AddMark(_currentPlayer.Mark, num) == false)
                {
                    Console.WriteLine("Nope!  Your coordinate is already taken.");
                    DoAllAgain = false;
                }
                else
                    DoAllAgain = true;
                Console.Clear();
                _board.Display();
            } while (!DoAllAgain);  
        }

        private void SetUp()
        {
            Console.WriteLine("Welcome to Tic-Tac-Toe! Hold onto your hats! \nPress enter to advance the screens.");
            Console.ReadLine();
            Console.Clear();
            _player1 = CreatePlayer(1, "X");
            _player2 = CreatePlayer(2, "O");
        }

        private void NextPlayer()
        {
            if (_currentPlayer == null || _currentPlayer.Number == 2)
            {// start of game
                _currentPlayer = _player1;
            }
            else
            {
                _currentPlayer = _player2;
            }
        }

        private Player CreatePlayer(int number, string mark)
        {
            Player player = new Player();
            Console.WriteLine("Hello!  Player {0}, please enter your name.", number);
            player.Name = Console.ReadLine();
            player.Mark = mark;
            player.Number = number;
            return player;
            
        }

        private void PlayAgain()
        {
            string response;
            Console.WriteLine("Would you like to play again?  If yes, hit 'Y' if not, enter any other character or number");
            response = Console.ReadLine();
            if ((response == "Y") || (response == "y"))
            {
                WillPlay = 0;
                Console.WriteLine("Great! Please 'Enter' and we'll play again!");
                Console.ReadLine();
                Console.Clear();
                _board = new Board();
            }
            else
                WillPlay = 1;
        }
    }
}
