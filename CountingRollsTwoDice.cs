﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RollTwoDiceCounter
{
    class Program
    {
        public static int two;
        public static int three;
        public static int four;
        public static int five;
        public static int six;
        public static int seven;
        public static int eight;
        public static int nine;
        public static int ten;
        public static int eleven;
        public static int twelve;

        static void Main(string[] args)
        {
            List<int> ListNums = new List<int>();
            Random rnd = new Random();

            for (int i = 0; i < 100; i++)
            {
                int Die1 = rnd.Next(1, 7);
                int Die2 = rnd.Next(1, 7);
                int num = Die1 + Die2;
                ListNums.Add(num);
            }

            foreach (var num in ListNums)
            {
                switch (num)
                {
                    case 2:
                        two++;
                        break;
                    case 3:
                        three++;
                        break;
                    case 4:
                        four++;
                        break;
                    case 5:
                        five++;
                        break;
                    case 6:
                        six++;
                        break;
                    case 7:
                        seven++;
                        break;
                    case 8:
                        eight++;
                        break;
                    case 9:
                        nine++;
                        break;
                    case 10:
                        ten++;
                        break;
                    case 11:
                        eleven++;
                        break;
                    case 12:
                        twelve++;
                        break;
                    default:
                        break;
                }
            }
            Console.WriteLine("Out of 100 rolls of two dice:");
            Console.WriteLine("The total number of twos is {0}.", two);
            Console.WriteLine("The total number of threes is {0}.", three);
            Console.WriteLine("The total number of fours is {0}.", four);
            Console.WriteLine("The total number of fives is {0}.", five);
            Console.WriteLine("The total number of sixes is {0}.", six);
            Console.WriteLine("The total number of sevens is {0}.", seven);
            Console.WriteLine("The total number of eights is {0}.", eight);
            Console.WriteLine("The total number of nines is {0}.", nine);
            Console.WriteLine("The total number of tens is {0}.", ten);
            Console.WriteLine("The total number of elevens is {0}.", eleven);
            Console.WriteLine("The total number of twelves is {0}.", twelve);
            Console.ReadLine();
        }
    }
}
