﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarDealership.Models;

namespace CarDealership.Repositories
{
    public class EFCarRepository : ICarRepository
    {
        public void AddCar(Car car)
        {
            throw new NotImplementedException();
        }

        public void DeleteCar(int carId)
        {
            throw new NotImplementedException();
        }

        public void EditCar(Car car)
        {
            throw new NotImplementedException();
        }

        public List<Car> GetAllCars()
        {
            using(CarDBEntities entities = new CarDBEntities())
            {
                List<Car> carList = new List<Car>();

                var cars = entities.CarEFs;
                foreach (CarEF ca in cars)
                {
                    carList.Add(new Car()
                    {
                        Make = ca.Make,
                        Model = ca.Model,
                        Description = ca.Description,
                        Title = ca.Title,
                        Year = ca.Year,
                        ImageUrl = ca.ImageUrl,
                        Price = ca.Price
                    });
                }

                return carList;
            }
        }

        public Car GetCarById(int id)
        {
            using(CarDBEntities entities = new CarDBEntities())
            {
                Car carById = new Car();
                var cars = entities.CarEFs;
                foreach (CarEF ca in cars)
                {
                    if (ca.Id == id)
                    {
                        carById.Id = ca.Id;
                        carById.Make = ca.Make;
                        carById.Model = ca.Model;
                        carById.Description = ca.Description;
                        carById.Title = ca.Title;
                        carById.Year = ca.Year;
                        carById.ImageUrl = ca.ImageUrl;
                        carById.Price = ca.Price;
                    }
                }

                return carById;
            }
        }

        public Car GetCarByModel(string name)
        {
            using (CarDBEntities entities = new CarDBEntities())
            {
                Car carByModel = new Car();
                var cars = entities.CarEFs;
                foreach (CarEF ca in cars)
                {
                    if (ca.Model == name)
                    {
                        carByModel.Id = ca.Id;
                        carByModel.Make = ca.Make;
                        carByModel.Model = ca.Model;
                        carByModel.Description = ca.Description;
                        carByModel.Title = ca.Title;
                        carByModel.Year = ca.Year;
                        carByModel.ImageUrl = ca.ImageUrl;
                        carByModel.Price = ca.Price;
                    }
                }

                return carByModel;
            }
        }

        public Car GetCarDetails(string year, string make, string model)
        {
            using (CarDBEntities entities = new CarDBEntities())
            {
                Car carByDetails = new Car();
                var cars = entities.CarEFs;
                foreach (CarEF ca in cars)
                {
                    if (ca.Model == model && ca.Model == make && ca.Year == year)
                    {
                        carByDetails.Id = ca.Id;
                        carByDetails.Make = ca.Make;
                        carByDetails.Model = ca.Model;
                        carByDetails.Description = ca.Description;
                        carByDetails.Title = ca.Title;
                        carByDetails.Year = ca.Year;
                        carByDetails.ImageUrl = ca.ImageUrl;
                        carByDetails.Price = ca.Price;
                    }
                }

                return carByDetails;
            }
        }

        public User LoginUser(string username, string password)
        {
            return new User()
            {
                Password = password,
                UserId = 1,
                UserMessage = "Some message",
                Username = "fakeuser"
            };
        }
    }
}